package com.springapp.mvc;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 首页控制器
 * Created by robot on 2015/5/9.
 */
@Controller
@RequestMapping("/")
public class HomeController
{
    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String home(Model model)
    {
        model.addAttribute("message", new DateTime().toString());
        return "home";
    }
}
