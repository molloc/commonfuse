package com.springapp.mvc.rest;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 应用版本接口
 * Created by robot on 2015/5/9.
 */
@RestController
public class RestVersionController
{
    @RequestMapping("/version")
    public String version()
    {
        return JSON.toJSONString(ImmutableMap.builder().put("name", "commonfuse").put("version", 1.0F).build());
    }
}
